namespace ExampleServer
{
    using System;

    using GraphQL.Types;

    public class ExampleSchema : Schema
    {
        public ExampleSchema(IServiceProvider serviceProvider)
            : base(serviceProvider)
        {
            this.Query = new ExampleQuery();
            this.Mutation = new ExampleMutation();
        }
    }
}
