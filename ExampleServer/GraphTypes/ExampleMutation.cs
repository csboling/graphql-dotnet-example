namespace ExampleServer
{
    using System.Threading;

    using GraphQL;
    using GraphQL.Types;

    public class ExampleMutation : ObjectGraphType
    {
        private static int count = 0;

        public ExampleMutation()
        {
            this.Field<NonNullGraphType<IntGraphType>, int>()
                .Name("increment")
                .Description("Increment the server's shared counter. Returns the new value.")
                .Resolve(
                    ctx => Interlocked.Increment(ref count));

            this.Field<NonNullGraphType<IntGraphType>, int>()
                .Name("exchange")
                .Description("Exchange the server's shared counter for the given value, and return it.")
                .Argument<NonNullGraphType<IntGraphType>, int>(
                    "newValue",
                    "The new value to set the server's shared counter to.")
                .Resolve(
                    ctx => Interlocked.Exchange(ref count, ctx.GetArgument<int>("newValue")));
        }
    }
}
