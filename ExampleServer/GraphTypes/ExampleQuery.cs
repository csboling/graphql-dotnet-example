namespace ExampleServer
{
    using GraphQL;
    using GraphQL.Types;

    public class ExampleQuery : ObjectGraphType
    {
        public ExampleQuery()
        {
            this.Field<NonNullGraphType<IntGraphType>, int>()
                .Name("add")
                .Description("Add two numbers and return the result.")
                .Argument<NonNullGraphType<IntGraphType>, int>("left", "The left operand")
                .Argument<NonNullGraphType<IntGraphType>, int>("right", "The right operand")
                .Resolve(
                    ctx =>
                    {
                        var left = ctx.GetArgument<int>("left");
                        var right = ctx.GetArgument<int>("right");
                        return left + right;
                    });
        }
    }
}
