namespace ExampleServer
{
    using GraphQL.Instrumentation;
    using GraphQL.Server;
    using GraphQL.Server.Ui.Playground;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Extensions.Logging;
    using System.Text;

    public class Startup
    {
        public Startup(IWebHostEnvironment environment)
        {
            this.Environment = environment;
        }

        private IWebHostEnvironment Environment { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(
                sp =>
                {
                    var schema = new ExampleSchema(sp);
                    schema.FieldMiddleware.Use(new InstrumentFieldsMiddleware());
                    return schema;
                });
            services.AddGraphQL(
                (options, provider) =>
                {
                    options.EnableMetrics = this.Environment.IsDevelopment();
                    var logger = provider.GetRequiredService<ILogger<ExampleSchema>>();
                    options.UnhandledExceptionDelegate = ctx =>
                        logger.LogError(ctx.OriginalException, "error during GraphQL execution");
                })
                .AddDefaultEndpointSelectorPolicy()
                .AddSystemTextJson()
                .AddErrorInfoProvider(
                    options =>
                    {
                        options.ExposeExceptionStackTrace = this.Environment.IsDevelopment();
                    })
                .AddGraphTypes(typeof(ExampleSchema));
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseStaticFiles();
            app.UseRouting();
            app.UseEndpoints(
                endpoints =>
                {
                    endpoints.MapGet(
                        "/",
                        async ctx =>
                        {
                            ctx.Response.StatusCode = 200;
                            ctx.Response.ContentType = "text/html";
                            await ctx.Response.Body.WriteAsync(
                                Encoding.UTF8.GetBytes(
                                    @"<html><body>Not much here, go to <a href=""/ui/playground"">/ui/playground</a> to experiment with the GraphQL API.</body></html>"),
                                ctx.RequestAborted);
                        });

                    endpoints.MapGraphQL<ExampleSchema>();
                    endpoints.MapGraphQLPlayground(
                        new PlaygroundOptions
                        {
                            HideTracingResponse = !env.IsDevelopment(),
                            SchemaPollingEnabled = false,
                        });
                });
        }
    }
}
